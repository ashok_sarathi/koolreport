<?php


require_once "../koolreport/autoload.php";

//Specify some data processes that will be used to process
use \koolreport\processes\Group;
use \koolreport\processes\Sort;
use \koolreport\processes\Limit;


class mydata extends \koolreport\KoolReport
{

    protected function settings()
    {
        //Define the "sales" data source which is the orders.csv 
        return array(
            "dataSources" => array(
                "student" => array(
                    "class" => '\koolreport\datasources\CSVDataSource',
                    "filePath" => "std.csv",
                ),
            )
        );
    }

    protected function setup()
    {
        //Select the data source then pipe data through various process
        //until it reach the end which is the dataStore named "sales_by_customer".
        $this->src('student')
        // ->pipe(new Group(array(
        //     "by"=>"customerName",
        //     "sum"=>"dollar_sales"
        // )))
            ->pipe(new Sort(array(
                "Age" => "desc"
            )))
            ->pipe(new Limit(array(40)))
            ->pipe($this->dataStore('students'));
    }
}

?>