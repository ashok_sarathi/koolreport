<?php 
    use \koolreport\widgets\koolphp\Table;
    use \koolreport\widgets\google\BarChart;
?>

<div class="report-content">

    <?php
    // BarChart::create(array(
    //     "dataStore"=>$this->dataStore('sales_by_customer'),
    //     "width"=>"100%",
    //     "height"=>"500px",
    //     "columns"=>array(
    //         "customerName"=>array(
    //             "label"=>"Customer"
    //         ),
    //         "dollar_sales"=>array(
    //             "type"=>"number",
    //             "label"=>"Amount",
    //             "prefix"=>"$",
    //             "emphasis"=>true
    //         )
    //     ),
    //     "options"=>array(
    //         "title"=>"Sales By Customer",
    //     )
    // ));
    ?>
    <?php
    Table::create(array(
        "dataStore"=>$this->dataStore('students'),
            "columns"=>array(
                "S.No"=>array(
                    "label"=>"S.No"
                ),
                "Name"=>array(
                    "label"=>"Name"
                ),
                "Age"=>array(
                    "label"=>"Age"
                ),
                "Mark"=>array(
                    "label"=>"Mark"
                ),
                "State"=>array(
                    "label"=>"State"
                )
            ),
        "cssClass"=>array(
            "table"=>"table table-hover table-bordered"
        )
    ));
    ?>
</div>