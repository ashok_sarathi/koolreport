<?php 


use \koolreport\widgets\koolphp\Table;
use \koolreport\widgets\google\BarChart;
?>
<div class="container">
<h2>All</h2>
<div class="report-content">
    <?php
    Table::create(array(
        "dataStore" => $this->dataStore('emps'),
        "columns" => array(
            "id" => array(
                "label" => "Id"
            ),
            "e_name" => array(
                "label" => "Name"
            ),
            "e_age" => array(
                "label" => "Age"
            ),
            "e_dep" => array(
                "label" => "Department"
            )
        ),
        "cssClass" => array(
            "table" => "table table-hover table-bordered"
        )
    ));
    ?>
</div>
<br>
<h2>Age Below 50 </h2>
<div class="report-content">
    <?php
    Table::create(array(
        "dataStore" => $this->dataStore('emps_bl_50'),
        "columns" => array(
            "id" => array(
                "label" => "Id"
            ),
            "e_name" => array(
                "label" => "Name"
            ),
            "e_age" => array(
                "label" => "Age"
            )
        ),
        "cssClass" => array(
            "table" => "table table-hover table-bordered"
        )
    ));
    ?>
</div>
<br>
<h2>Age group </h2>
<div class="report-content">
    <?php
    Table::create(array(
        "dataStore" => $this->dataStore('e_age_group'),
        "columns" => array(
            "e_age" => array(
                "label" => "Age"
            )
        ),
        "cssClass" => array(
            "table" => "table table-hover table-bordered"
        )
    ));
    ?>
</div>

<br>
<h2>sdl table group by </h2>
<div class="report-content">
    <?php
    Table::create(array(
        "dataStore" => $this->dataStore('sdldata'),
        "columns" => array(
            "com_id" => array(
                "label" => "Company"
            ),
            "amount" => array(
                "label" => "Amount"
            )
        ),
        "cssClass" => array(
            "table" => "table table-hover table-bordered"
        )
    ));
    ?>
</div>



<?php


\koolreport\widgets\google\ColumnChart::create(array(
    "title" => "Company amount",
    "dataSource" => $this->dataStore('sdldatagrp'),
    "columns" => array(
        "com_id",
        "amount" => array(
            "label" => "Amount",
            "type" => "number",
            "prefix" => "$"
        ),
    ),
));

?>

<?php 
\koolreport\widgets\google\BarChart::create(array(
    "title" => "Company amount",
    "dataSource" => $this->dataStore("sdldata"),
    "columns" => array(
        "com_id",
        "amount" => array("label" => "Sale", "type" => "number", "prefix" => "$"),
    ),
));
?>

<?php
\koolreport\widgets\google\PieChart::create(array(
    "title" => "Company amount",
    "dataSource" => $this->dataStore('sdldatagrp'),
    "columns" => array(
        "com_id",
        "amount" => array(
            "type" => "number",
            "label" => "Amount",
            "prefix" => "$",
        )
    )
));
?>

<?php
\koolreport\widgets\google\PieChart::create(array(
    "title" => "Company amount",
    "dataSource" => $this->dataStore('sdldatagrp'),
    "columns" => array(
        "com_id",
        "amount" => array(
            "type" => "number",
            "label" => "Amount",
            "prefix" => "$",
        )
    ),
    "options" => array(
        "is3D" => true
    )
));
?>

<?php
\koolreport\widgets\google\PieChart::create(array(
    "title" => "Company amount",
    "dataSource" => $this->dataStore('sdldatagrp'),
    "columns" => array(
        "com_id",
        "amount" => array(
            "type" => "number",
            "label" => "Amount",
            "prefix" => "$",
        )
    ),
    "options" => array(
        "is3D" => true,
        "slices"=>array(
            "1"=>array("offset"=>0.5)
        )
    )
));
?>
</div>
