<?php
require_once "../koolreport/autoload.php";

//Specify some data processes that will be used to process
use \koolreport\processes\Group;
use \koolreport\processes\Sort;
use \koolreport\processes\Limit;
use \koolreport\processes\Filter;
use \koolreport\processes\AggregatedColumn;

class test extends \koolreport\KoolReport
{
    protected function settings()
    {
        return array(
            "dataSources" => array(
                "data" => array(
                    "connectionString" => "mysql:host=192.168.0.99;port=3307;dbname=test",
                    "username" => "user",
                    "password" => "password",
                    "charset" => "utf8"
                ),
                "empsscsv" => array(
                    "class" => '\koolreport\datasources\CSVDataSource',
                    "filePath" => "empss.csv",
                ),
                "sdlcsv" => array(
                    "class" => '\koolreport\datasources\CSVDataSource',
                    "filePath" => "sdl.csv",
                ),
            )
        );
    }

    protected function setup()
    {

        // This codes are only for csv operations,  dataSources = "empsscsv"
        // =====================================
        $this->src('empsscsv')
            ->pipe(new Sort(array(
                "e_age" => "asc",
                "e_name" => "desc"
            )))
            ->pipe($this->dataStore("emps"));

        $this->src('empsscsv')
            ->pipe(new Filter(array(array("e_age", "<", 50))))
            ->pipe($this->dataStore("emps_bl_50"));

        $this->src('empsscsv')
            ->pipe(new Group(array(
                "by" => "e_age"
            )))
            ->pipe($this->dataStore("e_age_group"));


        // =====================================

         // This codes are only for csv operations,  dataSources = "sdlcsv"
        // =====================================
        $this->src('sdlcsv')
            ->pipe(new Group(array(
                "by" => "com_id",
                "sum" => "amount"
            )))
            ->pipe($this->dataStore("sdldatagrp"));

        $this->src('sdlcsv')
            ->pipe($this->dataStore("sdldata"));
        // =====================================



        // This codes are only for db operations,  dataSources = "data"
        // =====================================
        // $this->src('data')
        //     ->query("select * from empss")
        //     ->pipe(new Sort(array(
        //         "e_age" => "asc",
        //         "e_name" => "desc"
        //     )))
        //     ->pipe($this->dataStore("emps"));

        // $this->src('data')
        //     ->query("select * from empss")
        //     ->pipe(new Filter(array(array("e_age", "<", 50))))
        //     ->pipe($this->dataStore("emps_bl_50"));

        // $this->src('data')
        //     ->query("select e_age from empss")
        //     ->pipe(new Group(array(
        //         "by" => "e_age"
        //     )))
        //     ->pipe($this->dataStore("e_age_group"));

        // $this->src('data')
        //     ->query("select * from sdl")
        //     ->pipe(new Group(array(
        //         "by" => "com_id",
        //         "sum" => "amount"
        //     )))
        //     ->pipe($this->dataStore("sdldatagrp"));

        // $this->src('data')
        //     ->query("select com_id,amount from sdl")
        //     ->pipe($this->dataStore("sdldata"));
        // =====================================
    }
}
?>